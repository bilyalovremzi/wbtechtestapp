//
//  WBTechTestAppApp.swift
//  WBTechTestApp
//
//  Created by Ремзи Билялов on 29.06.2024.
//

import SwiftUI

@main
struct WBTechTestAppApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
